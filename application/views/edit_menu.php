<table width="100%" height="54" border="1">
  <tr>
    <td height="23" colspan="6" align="center" bgcolor="#3399FF"><b>TOKO JAYA ABADI</b></td>
  </tr>
  <tr bgcolor="#33CCFF">
    <td height="23" align="center"><li><a href="<?=base_url();?>main">Home</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>karyawan/listkaryawan">Master</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>jabatan/listjabatan">Transaksi</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>barang/listbarang">Report</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>jenis_barang/listjenisbarang">Logout</a></li></td>
  </table>
  
 
<div align="center">
  <h1>Edit Menu</h1></div>
  
  <?php
	foreach($edit_menu as $data) {
		$kode_menu		=$data->kode_menu;
		$nama_menu		=$data->nama_menu;
		$harga			=$data->harga;
		$keterangan		=$data->keterangan;
	}
?>
  
<form action="<?=base_url()?>menu/editmenu/<?=$kode_menu?>" method="POST">
<table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" >
  <tr>
    <td width="37%">Kode Menu</td>
    <td width="4%"> :</td>
    <td width="59%">
      <input type="text" name="kode_menu" id="kode_menu" maxlength="50" value="<?=$kode_menu;?>">
    </td>
  </tr>
  <tr>
    <td>Nama Menu</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_menu" id="nama_menu" maxlength="50" value="<?=$nama_menu;?>">
    </td>
  </tr>
 <tr>
   <td width="37%">Harga</td>
   <td width="4%"> :</td>
   <td width="59%">
     <input type="text" name="harga" id="harga" maxlength="50" value="<?=$harga;?>">
     </td>
 </tr>
  <tr>
    <td>Keterangan</td>
    <td>:</td>
    <td>
      <textarea name="keterangan" id="keterangan" cols="45" rows="5" ><?=$keterangan;?></textarea>
   </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="submit" name="Submit" id="Submit" value="Simpan">
      <input type="reset" name="Batal" id="Batal" value="Batal">
      </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
    	<a href="<?=base_url();?>menu/listmenu">
      <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"> </a>
    </td>
  </tr>
</table>
</form>
