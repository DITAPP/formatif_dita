<table width="100%" height="54" border="1">
  <tr>
    <td height="23" colspan="6" align="center" bgcolor="#3399FF"><b>TOKO PIZZA</b></td>
  </tr>
  <tr bgcolor="#33CCFF">
    <td height="23" align="center"><li><a href="<?=base_url();?>Home">Home</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>karyawan/listkaryawan">Master</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>">Transaksi</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>">Report</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>">Logout</a></li></td>
  </table> 
 
<div align="center">
  <h1>Edit Master Karyawan</h1></div>
  
  <?php
	foreach($edit_karyawan as $data) {
		
		
		$nik			=$data->nik;
		$nama			=$data->nama;
		$tempat_lahir	=$data->tempat_lahir;
		$tanggal_lahir	=$data->tanggal_lahir;
		$alamat			=$data->alamat;
		$telp			=$data->telp;
	}
	$thn_pisah = substr($tanggal_lahir, 0, 4);
	$bln_pisah = substr($tanggal_lahir, 5, 2);
	$tgl_pisah = substr($tanggal_lahir, 8, 2);
?>
  
<form action="<?=base_url()?>karyawan/editkaryawan/<?=$nik?>" method="POST">
<table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" >
  <tr>
    <td width="37%">NIK</td>
    <td width="4%"> :</td>
    <td width="59%">
      <input type="text" name="nik" id="nik" maxlength="50" value="<?=$nik;?>" >
    </td>
  </tr>
  <tr>
    <td>Nama </td>
    <td>:</td>
    <td>
      <input type="text" name="nama" id="nama" maxlength="50" value="<?=$nama;?>">
    </td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>
      <textarea name="alamat" id="alamat" cols="45" rows="5" > <?=$alamat;?></textarea>
   </td>
  </tr>
  <tr>
    <td width="37%">Telepon</td>
    <td width="4%"> :</td>
    <td width="59%">
      <input type="text" name="telp" id="telp" maxlength="50" value="<?=$telp;?>">
    </td>
  </tr>
  <tr>
    <td width="37%">Tempat Lahir</td>
    <td width="4%"> :</td>
    <td width="59%">
      <input type="text" name="tempatlahir" id="tempatlahir" maxlength="50" value="<?=$tempat_lahir;?>">
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
      <select name="tanggal_lahir" id="tanggal_lahir">
      <?php
      	for($tgl=1;$tgl<=31;$tgl++){
			$select_tgl = ($tgl == $tgl_pisah)?'selected' : '' ;
		?>
        
      	<option value="<?=$tgl;?>" <?=$select_tgl;?>> <?=$tgl;?></option>
       <?php
       	}
       ?>
      </select>
      
      <select name="bln" id="bln">
      <?php
      	$bulan_n = array('Januari','Februari','Maret','April',
        				'Mei','Juni','Juli','Agustus','September',
                        'Oktober','November','Desember');
         for($bln=0;$bln<12;$bln++){
			 $select_bln = ($bln == $bln_pisah)?'selected' : '' ;
      ?>
        	<option value="<?=$bln+1;?>" <?=$select_bln;?>> <?=$bln;?></option>
        <?php
        	}
        ?>
      </select>
      
      <select name="thn" id="thn">
      <?php
      	for($thn = date('Y')-60;$thn<=date('Y')-15;$thn++){
			$select_thn = ($thn == $thn_pisah)?'selected' : '' ;
      ?>
        <option value="<?=$thn;?>" <?=$select_thn;?>> <?=$thn;?></option>
      <?php
      	} 
      ?>
      </select>
    </td>
  </tr
  ><tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="submit" name="Submit" id="Submit" value="Simpan">
      <input type="reset" name="Batal" id="Batal" value="Batal">
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
    	<a href="<?=base_url();?>karyawan/listkaryawan">
      <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"> </a>
    </td>
  </tr>
</table>
</form>
