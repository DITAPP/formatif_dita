<table width="100%" height="54" border="1">
  <tr>
    <td height="23" colspan="6" align="center" bgcolor="#3399FF"><b>TOKO JAYA ABADI</b></td>
  </tr>
  <tr bgcolor="#33CCFF">
    <td height="23" align="center"><li><a href="<?=base_url();?>main">Home</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>karyawan/listkaryawan">Master</a></li></td>
    <td align="center"><li><a href="<?=base_url();?>menu/listmenu">Menu</a></li></td>
  </table>
  
 
<div align="center"><h1>Input Master Karyawan</h1></div>
<form action="<?=base_url()?>karyawan/inputkaryawan" method="POST">
<table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" >
  <tr>
    <td width="37%">NIK</td>
    <td width="4%"> :</td>
    <td width="59%">
      <input type="text" name="nik" id="nik" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Nama </td>
    <td>:</td>
    <td>
      <input type="text" name="nama" id="nama" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>
      <textarea name="alamat" id="alamat" cols="45" rows="5"></textarea>
   </td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td>
      <textarea name="telp" id="telp" cols="45" rows="5"></textarea>
   </td>
  </tr>
  <tr>
    <td width="37%">Tempat Lahir</td>
    <td width="4%"> :</td>
    <td width="59%">
      <input type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
      <select name="tanggal_lahir" id="tanggal_lahir">
      <?php
      	for($tgl=1;$tgl<=31;$tgl++){
      ?>
      	<option value="<?=$tgl;?>"><?=$tgl;?></option>
       <?php
       	}
       ?>
      </select>
      
      <select name="bln" id="bln">
      <?php
      	$bulan_n = array('Januari','Februari','Maret','April',
        				'Mei','Juni','Juli','Agustus','September',
                        'Oktober','November','Desember');
         for($bln=0;$bln<12;$bln++){
      ?>
      	<option value="<?=$bln+1;?>">
        	<?=$bulan_n[$bln];?>
        </option>
        <?php
        	}
        ?>
      </select>
      
      <select name="thn" id="thn">
      <?php
      	for($thn = date('Y')-60;$thn<=date('Y')-15;$thn++){
      ?>
      	<option value="<?=$thn;?>"><?=$thn;?></option>
      <?php
      	} 
      ?>
      </select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="submit" name="Submit" id="Submit" value="Simpan">
      <input type="reset" name="Batal" id="Batal" value="Batal">
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
    	<a href="<?=base_url();?>karyawan/listkaryawan">
      <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"> </a>
    </td>
  </tr>
</table>
</form>
