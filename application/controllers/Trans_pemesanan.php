<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trans_pemesanan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("trans_pemesanan_model");
		$this->load->model("karyawan_model");
		$this->load->model("menu_model");
	}
	
	public function index()
	{
		$this->listtranspemesanan();
	}
	
	public function listtranspemesanan()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['data_menu'] = $this->menu_model->tampilDataMenu();
		$data['data_pemesanan'] = $this->trans_pemesanan_model->tampilDataTransaksiPemesanan2();
		$this->load->view('home_trans_pemesanan', $data);
	}
	
	public function input($id='')
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['data_menu'] = $this->menu_model->tampilDataMenu();
		$data['data_pemesanan'] = $this->trans_pemesanan_model->tampilDataTransaksiPemesanan2();
		
		if (!empty($_REQUEST)) {
			$m_pemesanan = $this->trans_pemesanan_model;
			@$m_pemesanan->savepemesanan($id);
			redirect("index.php/trans_pemesanan/listtranspemesanan", "refresh");
			
		}
		$this->load->view('input_trans_pemesanan', $data);
	}

}
